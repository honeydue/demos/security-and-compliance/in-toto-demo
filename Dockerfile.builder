ARG GO_VER=1.17

FROM golang:${GO_VER}-alpine AS intotobuild
ARG INTOTO_VER=0.3.0
WORKDIR /src
RUN apk --no-cache add curl make
RUN curl -Lo intoto.tar.gz https://github.com/in-toto/in-toto-golang/archive/refs/tags/v${INTOTO_VER}.tar.gz && \
  tar axf ./intoto.tar.gz && \
  cd in-toto-golang-${INTOTO_VER} && \
  CGO_ENABLED=0 make build && \
  mv bin/in-toto /src/

FROM golang:${GO_VER}-alpine
RUN apk --no-cache add git docker
COPY --from=intotobuild /src/in-toto /usr/local/bin/
